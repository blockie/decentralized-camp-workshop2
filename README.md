RaiseFundsForACause contract
============================

How it works
------------
The "Raise Funds For a Cause" _Ethereum_ contract offers a way for participants to donate, during a limited period of time, for a particular cause defined by a message string, addressed to a single beneficiary. The contract starts as "yet to be claimed" and an expiration timer is initiated. All those parameters are defined during contract creation.


Beneficiary setup
-----------------
Following deployment, the initial setup has to be performed by the beneficiary, the one that will be receiving the funds. That address defines what is the minimum amount of funds required for reaching the donation goal, calling the `receiverSetAmountRequired` function and passing the required value in wei. This can only be set once.


Donating
--------
After the initial setup, participants can start sending funds to the contract, via the payable `donate` function, specifying the amount in wei in the Value field. Donations are only accepted after the `receiverSetAmountRequired` has been called and are valid until the time limit period hasn't been reached and the funds haven't been claimed by the beneficiary.  
The receiving part of the contract is not allowed to donate. 


Statistics
----------
Public donation statistics are available through `totalAmountRaised` and `numPayments` counters, which are added every time a new donation comes in. If the contract raises more funds than the amount of funds required by the receiver, the difference will be reported at any time by the `currentTotalExcess` function, in wei.
The expiration timestamp can be read by calling the `expirationTimestamp` function, which returns the UNIX timestamp.


Withdraw
--------
If the contract times out and the funds haven't been claimed by the beneficiary, those who donated can choose to withdraw their funds with the `withdraw` function.  
In case the contract raises at least the required amount goal before the time limit, the receiver can drain all funds by calling the `receiverWithdraw` function. The contract will set `hasBeenClaimed` to `true` after the beneficiary successfully called `receiverWithdraw`.
