Create an account on Rinkeby test network using MyEtherWallet (MEW)
-------------------------------------------------------------------

1 - Go to: https://www.myetherwallet.com/

2 - On the top right side, select _Network: Rinkeby_

3 - Follow the on-screen instructions to create a new wallet

4 - Get funds to the newly created account using the following supplier:  
https://www.rinkeby.io/#faucet  
Follow the on-screen instructions to receive _Ether_. One way to do this is to post a tweet containing the wallet address and share it with the faucet application to receive 3 Ether.

5 - Visit _View Wallet Info_ on _MEW_ or _Etherscan_ to confirm the funds have been added:  
https://www.myetherwallet.com/#view-wallet-info or https://rinkeby.etherscan.io/address/0x...


Deploy the Raise Funds contract on Rinkeby
------------------------------------------

1 - Compile the `RaiseFundsForACause.sol` contract with _Remix IDE_ or _solc_.
For _Remix IDE_, go to https://remix.ethereum.org/, load the source code and wait for it to compile. Then, on the _Run_ tab, fill in the desired parameters next to the _Create_ button. For example:  
```sh
"0xeF0D4Af8c77A15966d67DB86F6Add902229dc6B1", "A test cause for blund", 1
```

After that, press the _Create_ button.
Once the contract is deployed, go to the _Console_ window at the bottom of the IDE and click "Details" on the transaction that called the constructor, which will read something like this:  
```sh
[vm] from:0xca3...a733c, to:browser/RaiseFundsForACause.sol:RaiseFundsForACause.(constructor)
```

Copy the generated input from the section with the same name.


2 - Go to: https://www.myetherwallet.com/

3 - On the top right side, select _Network: Rinkeby_

4 - Go to "Contracts" section and select "Deploy Contract"

5 - Paste the bytecode input from step 1 into the "Byte Code" text field. The Gas Limit will be automatically filled.

6 - Unlock the account by choosing one of the unlocking mechanisms e.g. private key

7 - Click the "Sign transaction" button and then "Deploy Contract"


Interact with the Raise Funds contract on Rinkeby
-------------------------------------------------

1 - Compile the `RaiseFundsForACause.sol` contract with _Remix IDE_ or _solc_.
For _Remix IDE_, go to https://remix.ethereum.org/, load the source code and wait for it to compile. Then, in the _Compile_ tab, click the _Details_ button and copy the "Interface - ABI".

2 - Go to: https://www.myetherwallet.com/

3 - On the top right side, select _Network: Rinkeby_

4 - Go to "Contracts" section and select "Interact with contract".
Fill in the contract address and paste the ABI from step 1.

5 - A "Read / Write Contract" menu will appear at the bottom of the page, along with a list of possible operations that can be performed.


Ropsten Bonus
-------------

List of suppliers for Ropsten network (mostly broken?):

a. http://faucet.ropsten.be:3001/  
b. `curl http://faucet.ropsten.be:3001/donate/0x...`  
c. https://ipfs.io/ipfs/QmVAwVKys271P5EQyEfVSxm7BJDKWt42A2gHvNmxLjZMps/  
d. `curl -X POST  -H "Content-Type: application/json" -d '{"toWhom":"0x..."}' https://ropsten.faucet.b9lab.com/tap`  
e. https://ethtools.com/ropsten/tools/faucet/  
