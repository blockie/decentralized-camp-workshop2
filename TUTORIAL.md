# Decentralized Camp Workshop 2

We will be using [Remix](https://remix.ethereum.org), which is a browser-based _Solidity_ compiler and integrated development environment (IDE).  


## Offline usage
It is also possible to download it from the [source repository](https://github.com/ethereum/browser-solidity) and run locally.  
Direct link for offline usage: https://github.com/ethereum/browser-solidity/raw/gh-pages/remix-e6f4dc2.zip
Download and extract the _zip_ file and open `index.html`.


## Structure of a Solidity file

A Solidity source file, usually followed by `.sol` extension, can contain one or more contracts written in Solidity language.

For those familiar with other programming languages, a contract is similar to a `class` in object-oriented programming languages. Each contract can express a set of data and a set of functions, in its most basic form.  
Let's look at a simple example:

```
/*
   My first source file!
   This is a multi line, block comment.
   Comments are ignored by the compiler.
*/
contract MyContract
{
    // This is a different comment, in a single line
    uint public data;

    // A function to simply return data
    function dataOperation() public returns(uint) {
        return data;
    }
}
```

Solidity is not limited to just data and functions as way of expressing code. However, that is the foundation and most common form of expressing data storage and performing operations on data.


## Understanding the Raise Funds contract

The _Raise Funds_ contract offers a way for participants to donate _ether_, during a limited period of time, for a particular cause, addressed to a single beneficiary.

After the contract has been deployed, the initial setup has to be performed by the beneficiary, the one that will be receiving the funds. That address defines what is the minimum amount of funds required for reaching the donation goal, calling the `receiverSetAmountRequired` function. That function also starts a timer which limits the period donations can be accepted.

After that, participants can start sending ether to the contract, via the `donate` function. The receiving part of the contract is not allowed to donate. Donations are only accepted after the `receiverSetAmountRequired` has been called and are valid until the time limit period hasn't been reached.

Public donation statistics are available through `totalAmountRaised` and `numPayments` counters, which are added every time a new donation comes in. If the contract raises more funds than the amount of funds required by the receiver, the difference will be reported at any time by the `currentTotalExcess` function in wei.

If the contract times out and the total amount raised hasn't reached the minimum amount required by the receiver, those who donated can choose to withdraw their funds with the `withdraw` function.
In case the contract raises at least the required amount goal before the time limit, the receiver can drain all funds by calling the `receiverWithdraw` function. The contract is destroyed at the end of the process.


## Remix: using an integrated development environment

_Remix IDE_ can be divided into four main parts. The file browser on the left side; a text editor presented on the central column, an output console at the bottom screen and a panel for parameter settings on the right side.


### Load a source file

Using the file browser it is possible to load a file from the computer or add an empty one pressing the "plus" button.  
Here we load the _Raise Funds_ contract, either by pasting the file contents to the text editor or loading it from an existing file on local disk.


### Compiling a contract source file

By default, the _IDE_ compiles, from time to time, the source code currently being edited. It is possible to turn off that setting on the _Compile_ tab under the rightmost settings panel.


### Running a contract

On the settings panel, the _Run_ tab takes care of all settings needed for deploying and running a contract.

#### Environment
By default, the `JavaScript VM` setting will be selected, resulting in operations being run in a simulated environment provided by _Remix_.  
Other options are `Injected Web3`, which connects to third-party services and `Web3 Provider`, which enables connecting to an existing node, like the one provided on _Workshop #1_.

#### Account
Offers a predefined and seeded list of accounts for using. The selected account is the one which will perform the transactions, from the contract deployment to function calls. It is possible to select a different account at any time.

#### Gas limit
Defines the maximum quantity of gas in _wei_ used to perform the transactions.

#### Value
Defines the amount of _ether_ to send with a given transaction.

#### At address
This enables using an existing contract at a given address.

#### Create
Allows deploying the current compiled contract into the environment.


After analyzing the options, we deploy the contract filling up the parameters accordingly:
```
"0x14723a09acff6d2a60dcdf7aa4aff308fddc160c", "This is for a good cause", 1
```


### Running contract operations

Now that the contract has been deployed, all public variables and functions will appear on a sub-panel right after the _Create_ button from the previous step.
Blue buttons will show the public data and red buttons will preset as the public functions available (operations).


#### Set amount required
Now, let's run our very first function call. Select the beneficiary account `0x14723a09acff6d2a60dcdf7aa4aff308fddc160c` and set the `receiverSetAmountRequired` parameter as `"2000000000000000000"`, representing 2 _ether_. Press the function button and wait for the transaction to complete.

Hint: experiment with the _Debug_ button that appears along with the transaction details on the _Output Console_.


#### Donate
Select any account other than the `0x14723a09acff6d2a60dcdf7aa4aff308fddc160c`, which represents the beneficiary in this example, and let's try sending a donation. Set the _Value_ field to `1` and click the `donate` button.  
Now, inspect the public data such as `currentTotalExcess`, `totalAmountRaised`, `numPayments` and `minimumAmountRequired`, to confirm that the donation has been processed.  
The total amount raised should read `1000000000000000000` (1 _ether_) and the number of payments should now also be `1`. Currently there should be no excess because the minimum amount required has not been reached.  

Repeat the donation but set the _Value_ to `2` this time. The goal will be reached and the total excess amount will now read `1000000000000000000` (1 _ether_) and total amount raised will be `3000000000000000000` (3 _ether_), as expected.


#### Withdrawing all the funds

Since the goal has been reached, the beneficiary will be able to withdraw all the funds and close the contract, provided the contract has not expired.  
Select the `0x14723a09acff6d2a60dcdf7aa4aff308fddc160c` account and call the `receiverWithdraw` function.  
The account should now be filled with the total amount of funds raised. After that, the contract is destroyed and becomes inaccessible.


/TUTORIAL
